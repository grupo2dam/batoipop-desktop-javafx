package es.batoi.desktopapp;

import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import style.ButtonStyle;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;

public class MainScreen implements Initializable {

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private Button btCategory;

    @FXML
    private Button btComentarios;

    @FXML
    private Button btDenuncias;

    @FXML
    private Button btEtiquetas;

    @FXML
    private Button btListas;

    @FXML
    private Button btUsers;

    @FXML
    private Button btCerrar;

    @FXML
    private ImageView imgCategory;

    @FXML
    private ImageView imgComentarios;

    @FXML
    private ImageView imgDenuncias;

    @FXML
    private ImageView imgEtiquetas;

    @FXML
    private ImageView imgListas;

    @FXML
    private ImageView imgUsers;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        imgCategory.setImage(new Image("File:img/category.png"));
        imgEtiquetas.setImage(new Image("File:img/etiquetas.png"));
        imgUsers.setImage(new Image("File:img/userIcon.png"));
        imgDenuncias.setImage(new Image("File:img/denuncias.png"));
        imgComentarios.setImage(new Image("File:img/comentarios.png"));
        imgListas.setImage(new Image("File:img/listas.png"));

        ButtonStyle.buttonStyle(btCerrar, true);

        btCategory.setOnMouseClicked(mouseEvent -> {
            openScene(mouseEvent, "categorias.fxml");
        });

        btEtiquetas.setOnMouseClicked(mouseEvent -> {
            openScene(mouseEvent, "etiquetas.fxml");
        });

        btUsers.setOnMouseClicked(mouseEvent -> {
            openScene(mouseEvent, "usuarios.fxml");
        });

        btDenuncias.setOnMouseClicked(mouseEvent -> {
            openScene(mouseEvent, "denuncias.fxml");
        });

        btComentarios.setOnMouseClicked(mouseEvent -> {
            openScene(mouseEvent, "comentarios.fxml");
        });

        btListas.setOnMouseClicked(mouseEvent -> {
            openScene(mouseEvent, "listas.fxml");
        });

    }

    Stage getStage(ActionEvent event){
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        return stage;
    }

    public void adjustUI(WindowEvent event){
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        if (!stage.isFullScreen()){
            stage.setFullScreen(true);
        }
    }

    void openScene (MouseEvent mouseEvent, String fxmlFile) {

        try {
            Parent root = FXMLLoader.load(Objects.requireNonNull(DenunciasController.class.getResource(fxmlFile)));
            Stage stage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
            Scene scene = new Scene(root);
            scene.getStylesheets().addAll(Objects.requireNonNull(this.getClass().getResource("css/mainScreen.css")).toExternalForm());
            stage.setTitle("BatoiPop");
            stage.setFullScreen(true);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @FXML
    void goBack(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(Objects.requireNonNull(MainScreen.class.getResource("login.fxml")));
            Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            Scene scene = new Scene(root);
            scene.getStylesheets().addAll(Objects.requireNonNull(this.getClass().getResource("css/login.css")).toExternalForm());
            stage.setTitle("BatoiPop");
            stage.setFullScreen(true);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
