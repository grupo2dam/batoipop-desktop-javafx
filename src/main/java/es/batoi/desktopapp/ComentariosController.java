package es.batoi.desktopapp;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import modelo.comentarios.Comentarios;
import modelo.comentarios.ComentariosProperty;
import style.ButtonStyle;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class ComentariosController {

    @FXML
    private ImageView btBack;

    @FXML
    private TableView<ComentariosProperty> tblComentarios;

    @FXML
    private TableColumn<ComentariosProperty, String> colArticulo;

    @FXML
    private TableColumn<ComentariosProperty, String> colUsuario;

    @FXML
    private TableColumn<ComentariosProperty, String> colComentario;

    @FXML
    private TableColumn<ComentariosProperty, ComentariosProperty> colEditar;

    @FXML
    private TextField tfBuscar;

    List<Comentarios> comentarios = new ArrayList<>();

    Comentarios c1 = new Comentarios(1, "Daniel", "Me ha parecido bien");
    Comentarios c2 = new Comentarios(2, "Margaret", "No me ha parecido bien");
    
    @FXML
    private void initialize() {
        rellenaTabla();
    }
    
    private void rellenaTabla() {

        try {
            
            comentarios.add(c1);
            comentarios.add(c2);
            
            List<ComentariosProperty> comentariosPropertyList = new ArrayList<>();

            for (Comentarios c : comentarios) {
                comentariosPropertyList.add(new ComentariosProperty(c));
            }

            ObservableList<ComentariosProperty> data = FXCollections.observableArrayList(comentariosPropertyList);

            colArticulo.setCellValueFactory(new PropertyValueFactory<>("articulo"));
            colUsuario.setCellValueFactory(new PropertyValueFactory<>("usuario"));
            colComentario.setCellValueFactory(new PropertyValueFactory<>("comentario"));
            colEditar.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
            colEditar.setCellFactory(param -> new TableCell<>() {
                private final Button borrarButton = new Button("Deshabilitar");


                @Override
                protected void updateItem(ComentariosProperty comentario, boolean empty) {
                    super.updateItem(comentario, empty);

                    if (comentario == null) {
                        setGraphic(null);
                        return;
                    }

                    setGraphic(borrarButton);
                    ButtonStyle.buttonStyle(borrarButton, false);

                    borrarButton.setPrefWidth(Double.MAX_VALUE);
                    borrarButton.setOnAction(event -> {
                        if (mensajeConfirmacion())
                        data.remove(comentario);
                    });
                }
            });

            FilteredList<ComentariosProperty> filteredData = new FilteredList<>(data, a->true);

            tfBuscar.textProperty().addListener((observable, oldValue, newValue) -> filteredData.setPredicate(comentario -> {

                if (newValue == null || newValue.isEmpty())
                    return true;

                String lowerCaseFilter = newValue.toLowerCase(Locale.ROOT);

                if (comentario.getUsuario().toLowerCase(Locale.ROOT).contains(lowerCaseFilter)) {
                    return true;
                } else {
                    return false;
                }

            }));

            SortedList<ComentariosProperty> sortedData = new SortedList<>(filteredData);

            tblComentarios.setItems(sortedData);
            
        } catch (NullPointerException e) {
            tblComentarios = new TableView<>();
        }
        
        
    }

    private boolean mensajeConfirmacion() {

        try {
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("confirmacion.fxml"));
            Parent root = loader.load();
            ConfirmacionController controller = loader.getController();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setTitle("batoipop");
            stage.showAndWait();
            return controller.getOption();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return false;

    }

    @FXML
    public void goBack(MouseEvent event) {

        try {
            Parent root = FXMLLoader.load(Objects.requireNonNull(MainScreen.class.getResource("mainScreen.fxml")));
            Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            Scene scene = new Scene(root);
            scene.getStylesheets().addAll(Objects.requireNonNull(this.getClass().getResource("css/mainScreen.css")).toExternalForm());
            stage.setTitle("BatoiPop");
            stage.setFullScreen(true);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
