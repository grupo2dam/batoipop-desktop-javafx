package es.batoi.desktopapp;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public class ListasController {

    private static String LISTA_1 = "Generar lista de\nusuarios más activos";
    private static String LISTA_2 = "Generar lista de\nusuarios mejores\nvalorados";
    private static String LISTA_3 = "Generar lista de\ncategorias con mas\nventas";

    @FXML
    private Button btLista1;

    @FXML
    private Button btLista2;

    @FXML
    private Button btLista3;

    @FXML
    private void initialize() {
        btLista1.setText(LISTA_1);
        btLista2.setText(LISTA_2);
        btLista3.setText(LISTA_3);
    }

    public void goBack(MouseEvent event) {
        try {
            Parent root = FXMLLoader.load(Objects.requireNonNull(MainScreen.class.getResource("mainScreen.fxml")));
            Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            Scene scene = new Scene(root);
            scene.getStylesheets().addAll(Objects.requireNonNull(this.getClass().getResource("css/mainScreen.css")).toExternalForm());
            stage.setTitle("BatoiPop");
            stage.setFullScreen(true);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
