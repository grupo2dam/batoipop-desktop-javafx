package es.batoi.desktopapp;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import style.ButtonStyle;

public class ConfirmacionController {

    @FXML
    private Button btNo;

    @FXML
    private Button btSi;

    @FXML
    private AnchorPane pane;

    private boolean option;

    public boolean getOption() {
        return option;
    }

    @FXML
    private void initialize () {
        pane.setStyle("-fx-background-color: #F3E2E2");

        ButtonStyle.buttonStyle(btSi, true);
        ButtonStyle.buttonStyle(btNo, false);
    }

    Stage getStage(ActionEvent event){
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        return stage;
    }

    @FXML
    void opcionSi(ActionEvent event) {
        option = true;
        Stage stage = getStage(event);
        stage.close();
    }

    @FXML
    void opcionNo(ActionEvent event) {
        option = false;
        Stage stage = getStage(event);
        stage.close();
    }
}
