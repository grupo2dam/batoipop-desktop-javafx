package es.batoi.desktopapp;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public class App extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("login.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        
        scene.getStylesheets().addAll(Objects.requireNonNull(this.getClass().getResource("css/login.css")).toExternalForm());
        stage.setTitle("BatoiPop");
        stage.getIcons().add(new Image("File:img/icon.png"));
        stage.setScene(scene);
//        stage.setFullScreen(true);
        stage.setFullScreenExitHint("");
        stage.addEventHandler(KeyEvent.KEY_PRESSED, keyEvent -> {
            if (KeyCode.F11.equals(keyEvent.getCode())){
                stage.setFullScreen(!stage.isFullScreen());
            }
        });
        stage.setResizable(true);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}