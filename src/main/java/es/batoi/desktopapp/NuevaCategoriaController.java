package es.batoi.desktopapp;

import api.CategoriasAPI;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import modelo.articulos.Articulos;
import modelo.articulos.ArticulosProperty;
import modelo.categorias.Categorias;
import style.ButtonStyle;

import javax.xml.catalog.Catalog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class NuevaCategoriaController {

    public static final int INSERT = 0;
    public static final int UPDATE = 1;

    private int method = INSERT;

    @FXML
    private TextField tfId;

    @FXML
    private TextField tfNombre;

    @FXML
    private TextArea tfDescripcion;

    @FXML
    private ImageView igImagen;

    @FXML
    private TableView<ArticulosProperty> tblArticulos;

    @FXML
    private TableColumn<ArticulosProperty, String> colId;

    @FXML
    private TableColumn<ArticulosProperty, String> colNombre;

    @FXML
    private TableColumn<ArticulosProperty, ArticulosProperty> colBorrar;

    Articulos a1 = new Articulos(1, "Collar", "Es un collar", "10 años", 50f);
    Articulos a2 = new Articulos(2, "Bolsa de papas", "La acabo de comprar", "10 minutos", 2f);

    List<Articulos> articulos = new ArrayList<>();

    @FXML
    private Button btDescartar;

    @FXML
    private Button btGuardar;

    CategoriasAPI api = new CategoriasAPI();

    @FXML
    private void initialize() {
        rellenaTabla();
        btDescartar.setText("Descartar\nCambios");
        ButtonStyle.buttonStyle(btDescartar, true);
        ButtonStyle.buttonStyle(btGuardar, true);
    }

    public void goBack(MouseEvent event) {
        try {
            Parent root = FXMLLoader.load(Objects.requireNonNull(MainScreen.class.getResource("categorias.fxml")));
            Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            Scene scene = new Scene(root);
            scene.getStylesheets().addAll(Objects.requireNonNull(this.getClass().getResource("css/mainScreen.css")).toExternalForm());
            stage.setTitle("BatoiPop");
            stage.setFullScreen(true);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void rellenaTabla() {

        articulos.add(a1);
        articulos.add(a2);

        List<ArticulosProperty> articulosPropertyList = new ArrayList<>();

        for (Articulos a: articulos) {
            articulosPropertyList.add(new ArticulosProperty(a));
        }

        ObservableList<ArticulosProperty> data = FXCollections.observableArrayList(articulosPropertyList);

        colId.setCellValueFactory(new PropertyValueFactory<>("id"));
        colNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        colBorrar.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        colBorrar.setCellFactory(param -> new TableCell<>() {
            private final Button borrarButton = new Button("Borrar");


            @Override
            protected void updateItem(ArticulosProperty articulo, boolean empty) {
                super.updateItem(articulo, empty);

                if (articulo == null) {
                    setGraphic(null);
                    return;
                }

                setGraphic(borrarButton);
                ButtonStyle.buttonStyle(borrarButton, false);

                borrarButton.setPrefWidth(Double.MAX_VALUE);
//                borrarButton.setOnAction(event -> deleteArticle(event));
            }
        });

        tblArticulos.setItems(data);

    }

    @FXML
    private void saveUser(MouseEvent event) {
        if (method == INSERT) {
            api.insert(getCategoria());
        } else if (method == UPDATE) {
            api.update(getCategoria());
        }
        goBack(event);
    }

    private Categorias getCategoria () {

        String nombre = tfNombre.getText();
        String descripcion = tfDescripcion.getText();

        if (method == INSERT)
            return new Categorias(nombre, descripcion);
        else if (method == UPDATE) {
            int id = Integer.parseInt(tfId.getText());
            return new Categorias(id, nombre, descripcion);
        } else
            return null;
    }

    public void setCurrentData (String categoria) {

        method = UPDATE;

        int currentId = Integer.parseInt(categoria);
        System.out.println("Current id: " + currentId);

        tfId.setText(categoria);
        Categorias c = null;
        try {
            c = api.findByPK(currentId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assert c != null;
        tfNombre.setText(c.getNombre());
        tfDescripcion.setText(c.getDescripcion());


    }
}
