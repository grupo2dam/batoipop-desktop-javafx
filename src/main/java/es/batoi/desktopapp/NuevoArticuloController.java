package es.batoi.desktopapp;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import modelo.articulos.Articulos;
import style.ButtonStyle;

import java.io.IOException;
import java.util.Objects;

public class NuevoArticuloController {

    @FXML
    private Button btDescartar;

    @FXML
    private Button btGuardar;

    @FXML
    private ImageView igImagen;

    @FXML
    private TextField tfAntiguedad;

    @FXML
    private TextArea tfDescripcion;

    @FXML
    private TextField tfId;

    @FXML
    private TextField tfNombre;

    @FXML
    private TextField tfPrecio;

    @FXML
    private void initialize() {
        ButtonStyle.buttonStyle(btDescartar, false);
        ButtonStyle.buttonStyle(btGuardar, false);
    }

    @FXML
    public void goBack(MouseEvent event) {
        try {
            Parent root = FXMLLoader.load(Objects.requireNonNull(MainScreen.class.getResource("denuncias.fxml")));
            Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            Scene scene = new Scene(root);
            scene.getStylesheets().addAll(Objects.requireNonNull(this.getClass().getResource("css/mainScreen.css")).toExternalForm());
            stage.setTitle("BatoiPop");
            stage.setFullScreen(true);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void setCurrentData(String articulo) {



        tfId.setText(articulo);

    }

    @FXML
    public void saveUser() {



    }
}
