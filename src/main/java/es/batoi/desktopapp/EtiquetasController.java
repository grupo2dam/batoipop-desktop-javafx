package es.batoi.desktopapp;

import api.EtiquetasAPI;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import modelo.articulos.Articulos;
import modelo.articulos.ArticulosProperty;
import modelo.etiquetas.Etiquetas;
import modelo.etiquetas.EtiquetasProperty;
import style.ButtonStyle;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class EtiquetasController {

    @FXML
    private TableView<ArticulosProperty> tblArticulos;

    @FXML
    private TableColumn<ArticulosProperty, String> colId;

    @FXML
    private TableColumn<ArticulosProperty, String> colNombre;

    @FXML
    private TableColumn<ArticulosProperty, ArticulosProperty> colBorrar;

    static Articulos a1 = new Articulos(1, "Collar", "Es un collar", "10 años", 50f);
    static Articulos a2 = new Articulos(2, "Bolsa de papas", "La acabo de comprar", "10 minutos", 2f);

    List<Articulos> articulos = new ArrayList<>() {
        {
            add(a1);
            add(a2);
        }
    };

    List<Etiquetas> etiquetas = new ArrayList<>();

    @FXML
    private Button btGuardar;

    @FXML
    private TableView<EtiquetasProperty> tblEtiquetas;

    @FXML
    private TableColumn<EtiquetasProperty, String> colEtiqueta;

    @FXML
    private TableColumn<EtiquetasProperty, EtiquetasProperty> colEditarEtiqueta;

    @FXML
    private TextField tfEtiqueta;

    @FXML
    private TextField tfBuscar;

    EtiquetasAPI api = new EtiquetasAPI();


    @FXML
    private void initialize() {
        ButtonStyle.buttonStyle(btGuardar, true);
        rellenaTablaEtiquetas();
        rellenaTablaArticulos();

        restrictCharacters(tfEtiqueta);
    }

    @FXML
    private void rellenaTablaEtiquetas() {

        etiquetas = api.findAll();

        List<EtiquetasProperty> etiquetasPropertyList = new ArrayList<>();

        for (Etiquetas e: etiquetas) {
            etiquetasPropertyList.add(new EtiquetasProperty(e));
        }

        ObservableList<EtiquetasProperty> data = FXCollections.observableArrayList(etiquetasPropertyList);

        colEtiqueta.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        colEditarEtiqueta.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        colEditarEtiqueta.setCellFactory(param -> new TableCell<>() {
            private final Button borrarButton = new Button("Borrar");


            @Override
            protected void updateItem(EtiquetasProperty etiqueta, boolean empty) {
                super.updateItem(etiqueta, empty);

                if (etiqueta == null) {
                    setGraphic(null);
                    return;
                }

                setGraphic(borrarButton);
                ButtonStyle.buttonStyle(borrarButton, false);

                borrarButton.setPrefWidth(Double.MAX_VALUE);
                    borrarButton.setOnAction(event -> borraEtiqueta(etiqueta.getId()));
            }
        });

        tblEtiquetas.setItems(data);

    }

    private void rellenaTablaArticulos() {

        List<ArticulosProperty> articulosPropertyList = new ArrayList<>();

        for (Articulos a: articulos) {
            articulosPropertyList.add(new ArticulosProperty(a));
        }

        ObservableList<ArticulosProperty> data = FXCollections.observableArrayList(articulosPropertyList);

        colId.setCellValueFactory(new PropertyValueFactory<>("id"));
        colNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        colBorrar.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        colBorrar.setCellFactory(param -> new TableCell<>() {
            private final Button borrarButton = new Button("Borrar");


            @Override
            protected void updateItem(ArticulosProperty articulo, boolean empty) {
                super.updateItem(articulo, empty);

                if (articulo == null) {
                    setGraphic(null);
                    return;
                }

                setGraphic(borrarButton);
                ButtonStyle.buttonStyle(borrarButton, false);

                borrarButton.setPrefWidth(Double.MAX_VALUE);
                borrarButton.setOnAction(event -> {
                    if (mensajeConfirmacion())
                    data.remove(articulo);
                });
            }
        });

        FilteredList<ArticulosProperty> filteredData = new FilteredList<>(data, a->true);

        tfBuscar.textProperty().addListener((observable, oldValue, newValue) -> filteredData.setPredicate(articulo -> {

            if (newValue == null || newValue.isEmpty())
                return true;

            String lowerCaseFilter = newValue.toLowerCase(Locale.ROOT);

            if (articulo.getNombre().toLowerCase(Locale.ROOT).contains(lowerCaseFilter)) {
                return true;
            } else {
                return false;
            }

        }));



        SortedList<ArticulosProperty> sortedData = new SortedList<>(filteredData);

        tblArticulos.setItems(sortedData);

    }

    public void goBack(MouseEvent event) {
        try {
            Parent root = FXMLLoader.load(Objects.requireNonNull(MainScreen.class.getResource("mainScreen.fxml")));
            Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            Scene scene = new Scene(root);
            scene.getStylesheets().addAll(Objects.requireNonNull(this.getClass().getResource("css/mainScreen.css")).toExternalForm());
            stage.setTitle("BatoiPop");
            stage.setFullScreen(true);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void guardaEtiqueta() {

        Etiquetas e = new Etiquetas("#" + tfEtiqueta.getText());

        api.insert(e);
        rellenaTablaEtiquetas();
        tfEtiqueta.clear();

    }

    private void borraEtiqueta(String id) {

        if (mensajeConfirmacion()) {
            api.delete(Integer.parseInt(id));
            rellenaTablaEtiquetas();
        }

    }

    private boolean mensajeConfirmacion() {

        try {
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("confirmacion.fxml"));
            Parent root = loader.load();
            ConfirmacionController controller = loader.getController();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setTitle("batoipop");
            stage.showAndWait();
            return controller.getOption();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return false;

    }

    private void restrictCharacters(TextField textField) {
        tfEtiqueta.addEventFilter(KeyEvent.KEY_TYPED, keyEvent -> {
            if ("!\"#$%&'()*+,-./\\={}[];:_`^´¨*".contains(keyEvent.getCharacter())) {
                keyEvent.consume();
            }
        });
    }
}
