package es.batoi.desktopapp;

import api.CategoriasAPI;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import modelo.categorias.Categorias;
import modelo.categorias.CategoriasProperty;
import style.ButtonStyle;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class CategoriasController {

    @FXML
    private TextField tfBuscar;


    static List<Categorias> categoriasList = new ArrayList<>();

    @FXML
    private TableView<CategoriasProperty> tblCategorias;

    @FXML
    private TableColumn<CategoriasProperty, String> colNombre;

    @FXML
    private TableColumn<CategoriasProperty, String> colNumArticulos;

    @FXML
    private TableColumn<CategoriasProperty, String> colDescripcion;

    @FXML
    private TableColumn<CategoriasProperty, CategoriasProperty> colEditar;

    @FXML
    private TableColumn<CategoriasProperty, CategoriasProperty> colBorrar;

    @FXML
    private Button btNuevo;

    CategoriasAPI api = new CategoriasAPI();

    @FXML
    private void initialize() {
        ButtonStyle.buttonStyle(btNuevo, true);
        rellenaTabla();
    }

    @FXML
    private void createCategory(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(Objects.requireNonNull(MainScreen.class.getResource("nuevacategoria.fxml")));
            Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            Scene scene = new Scene(root);
            scene.getStylesheets().addAll(Objects.requireNonNull(this.getClass().getResource("css/mainScreen.css")).toExternalForm());
            stage.setTitle("BatoiPop");
            stage.setFullScreen(true);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void seeDetails(ActionEvent event, String categoria) {
            try {
                Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("nuevacategoria.fxml"));
                Parent root = loader.load();
                NuevaCategoriaController controller = loader.getController();
                controller.setCurrentData(categoria);
                Scene scene = new Scene(root);
                stage.setScene(scene);
                scene.getStylesheets().addAll(Objects.requireNonNull(this.getClass().getResource("css/mainScreen.css")).toExternalForm());
                stage.setTitle("batoipop");
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    public void rellenaTabla() {

        categoriasList = api.findAll();

        List<CategoriasProperty> categoriasPropertyList = new ArrayList<>();

        for (Categorias cat: categoriasList) {
            categoriasPropertyList.add(new CategoriasProperty(cat));
        }

        ObservableList<CategoriasProperty> data = FXCollections.observableArrayList(categoriasPropertyList);

        FilteredList<CategoriasProperty> filteredData = new FilteredList<>(data, a->true);

        colNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        colDescripcion.setCellValueFactory(new PropertyValueFactory<>("descripcion"));
        colEditar.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        colEditar.setCellFactory(param -> new TableCell<>() {
            private final Button editarButton = new Button("Editar");


            @Override
            protected void updateItem(CategoriasProperty categoria, boolean empty) {
                super.updateItem(categoria, empty);

                if (categoria == null) {
                    setGraphic(null);
                    return;
                }

                setGraphic(editarButton);
                ButtonStyle.buttonStyle(editarButton, false);

                editarButton.setPrefWidth(Double.MAX_VALUE);
                editarButton.setOnAction(event -> seeDetails(event, categoria.getId()));

            }
        });

        colBorrar.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        colBorrar.setCellFactory(param -> new TableCell<>() {
            private final Button borrarButton = new Button("Borrar");


            @Override
            protected void updateItem(CategoriasProperty categoria, boolean empty) {
                super.updateItem(categoria, empty);

                if (categoria == null) {
                    setGraphic(null);
                    return;
                }

                setGraphic(borrarButton);
                ButtonStyle.buttonStyle(borrarButton, false);

                borrarButton.setPrefWidth(Double.MAX_VALUE);
                borrarButton.setOnAction(event -> borrarCategoria(categoria.getId()));

            }
        });

        tfBuscar.textProperty().addListener((observable, oldValue, newValue) -> filteredData.setPredicate(categoria -> {

            if (newValue == null || newValue.isEmpty())
                return true;

            String lowerCaseFilter = newValue.toLowerCase(Locale.ROOT);

            if (categoria.getNombre().toLowerCase(Locale.ROOT).contains(lowerCaseFilter)) {
                return true;
            } else {
                return false;
            }

        }));

        SortedList<CategoriasProperty> sortedData = new SortedList<>(filteredData);

        tblCategorias.setItems(sortedData);

    }

    private void borrarCategoria(String id) {

        if (mensajeConfirmacion()) {
            api.delete(Integer.parseInt(id));
            rellenaTabla();
        }

    }

    private boolean mensajeConfirmacion() {

        try {
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("confirmacion.fxml"));
            Parent root = loader.load();
            ConfirmacionController controller = loader.getController();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setTitle("batoipop");
            stage.showAndWait();
            return controller.getOption();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return false;

    }

    public void goBack(MouseEvent event) {
        try {
            Parent root = FXMLLoader.load(Objects.requireNonNull(MainScreen.class.getResource("mainScreen.fxml")));
            Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            Scene scene = new Scene(root);
            scene.getStylesheets().addAll(Objects.requireNonNull(this.getClass().getResource("css/mainScreen.css")).toExternalForm());
            stage.setTitle("BatoiPop");
            stage.setFullScreen(true);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
