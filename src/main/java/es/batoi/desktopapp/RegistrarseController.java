package es.batoi.desktopapp;

import api.EmpleadoAPI;
import api.UsuariosAPI;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import modelo.empleados.Empleados;
import modelo.usuarios.Usuarios;
import style.ButtonStyle;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.util.Base64;
import java.util.Objects;

public class RegistrarseController {

    private static final String key = "grupo2damproyect";
    private static final String initVector = "proyectoIntegrad";
    private EmpleadoAPI empleadoApi;

    @FXML
    private PasswordField pfConfContrasenya;

    @FXML
    private PasswordField pfContrasenya;

    @FXML
    private TextField tfNombre;

    @FXML
    private Button btConfirmar;

    @FXML
    private Button btDescartar;

    @FXML
    private void initialize() {
        ButtonStyle.buttonStyle(btConfirmar, false);
        ButtonStyle.buttonStyle(btDescartar, false);

        String fontSize = "-fx-font-size: 33px";

        btDescartar.styleProperty().bind(Bindings
                .when(btDescartar.hoverProperty())
                .then(
                        new SimpleStringProperty("-fx-background-color: #8B0000; -fx-text-fill: white; -fx-font-family: Roundo; -fx-background-radius: 5px;" + fontSize)
                )
                .otherwise(
                        new SimpleStringProperty("-fx-background-color: #A60707; -fx-text-fill: white; -fx-font-family: Roundo; -fx-background-radius: 5px;" + fontSize)
                ));

    }

    public void goBack(MouseEvent event) {
        try {
            Parent root = FXMLLoader.load(Objects.requireNonNull(MainScreen.class.getResource("login.fxml")));
            Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            Scene scene = new Scene(root);
            scene.getStylesheets().addAll(Objects.requireNonNull(this.getClass().getResource("css/login.css")).toExternalForm());
            stage.setTitle("BatoiPop");
            stage.setFullScreen(true);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    @FXML
    void register(MouseEvent event) {
        StringBuilder sb = new StringBuilder();
        if (tfNombre.getText().equals("")){
            sb.append("Introduce un nombre de usuario valido\n");
        }
        if (pfContrasenya.getText().equals("") || pfConfContrasenya.getText().equals("")){
            sb.append("Introduce una contraseña correcta");
        }else if(!pfConfContrasenya.getText().equals(pfConfContrasenya.getText())){
            sb.append("Las dos contraseñas no coinciden");
        }
        String alerta = sb.toString();
        if (!alerta.equals("")){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Alerta. Informacion incorrecta");
            alert.setContentText(alerta);
            alert.showAndWait();
        }else{
            Empleados empleado = new Empleados(tfNombre.getText().toString(), encrypt(pfContrasenya.getText().toString()));
            empleadoApi = new EmpleadoAPI();
            empleadoApi.insert(empleado);
            goBack(event);
        }
    }

    public static String encrypt(String value) {
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            byte[] encrypted = cipher.doFinal(value.getBytes());
            return Base64.getEncoder().encodeToString(encrypted);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
