package es.batoi.desktopapp;

import api.EmpleadoAPI;
import api.UsuariosAPI;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import modelo.empleados.Empleados;
import modelo.usuarios.Usuarios;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.net.URL;
import java.util.Base64;
import java.util.Objects;
import java.util.ResourceBundle;

public class Login implements Initializable {

    private static final String key = "grupo2damproyect";
    private static final String initVector = "proyectoIntegrad";

    @FXML
    private VBox vboxLogin;

    @FXML
    private ImageView logo;

    @FXML
    private ImageView icoUser;

    @FXML
    private ImageView icoPasswd;

    @FXML
    private PasswordField tfPasswd;

    @FXML
    private TextField tfUser;

    @FXML
    private Text tError;

    @FXML
    void login(ActionEvent event) {

        if (tfUser.getText().equals("") || tfPasswd.getText().equals("")) {
            tError.setVisible(true);
            tError.setText("Error, introduce un usuario y una contraseña");
        } else{

           Empleados user;

           try {

               user = new EmpleadoAPI().login(tfUser.getText(), encrypt(tfPasswd.getText()));
               if (user == null) {
                   throw new RuntimeException("No hay conexión");
               }

               try {
                   Parent root = FXMLLoader.load(Objects.requireNonNull(MainScreen.class.getResource("mainScreen.fxml")));
                   Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                   Scene scene = new Scene(root);
                   scene.getStylesheets().addAll(Objects.requireNonNull(this.getClass().getResource("css/mainScreen.css")).toExternalForm());
                   stage.setTitle("BatoiPop");
                   stage.setFullScreen(true);
                   stage.setScene(scene);
                   stage.show();
               } catch (IOException e) {
                   e.printStackTrace();
               }

           } catch (RuntimeException e) {
               tError.setVisible(true);
               tError.setText(e.getMessage());
           }
        }
    }

    @FXML
    private void registrarse(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(Objects.requireNonNull(MainScreen.class.getResource("registrarse.fxml")));
            Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            Scene scene = new Scene(root);
            scene.getStylesheets().addAll(Objects.requireNonNull(this.getClass().getResource("css/mainScreen.css")).toExternalForm());
            stage.setTitle("BatoiPop");
            stage.setFullScreen(true);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        vboxLogin.setStyle("-fx-background-radius: 18 18 18 18;" +
                "    -fx-border-radius: 18 18 18 18;" +
                "    -fx-background-color: #F3E2E2;" +
                "    -fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.45), 10, 0.5, 0, 5);");
        logo.setImage(new Image("File:img/logo.png"));
        icoUser.setImage(new Image("File:img/userIcon.png"));
        icoPasswd.setImage(new Image("File:img/passwdIcon.png"));
        tError.setVisible(false);

        tfUser.focusedProperty().addListener((observableValue, oldValue, newValue) -> {
            if (newValue) {
                tError.setVisible(false);
            }
        });

        tfPasswd.focusedProperty().addListener((observableValue, oldValue, newValue) -> {
            if (newValue) {
                tError.setVisible(false);
            }
        });
    }

    @FXML
    void changeFullScreen(KeyEvent event) {
        if (event.getCharacter().equals("F11")){
            System.out.println("f11");
        }
    }

    public static String encrypt(String value) {
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            byte[] encrypted = cipher.doFinal(value.getBytes());
            return Base64.getEncoder().encodeToString(encrypted);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}