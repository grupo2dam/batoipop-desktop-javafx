package es.batoi.desktopapp;

import api.UsuariosAPI;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import modelo.usuarios.Usuarios;
import modelo.usuarios.UsuariosProperty;
import style.ButtonStyle;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class UsuariosController {

    @FXML
    private Button btNuevo;

    @FXML
    private TableColumn<UsuariosProperty, UsuariosProperty> colEditar;

    @FXML
    private TableColumn<UsuariosProperty, UsuariosProperty> colBorrar;

    @FXML
    private TableColumn<UsuariosProperty, String> colEmail;

    @FXML
    private TableColumn<UsuariosProperty, String> colNombre;

    @FXML
    private TableColumn<UsuariosProperty, String> colTelefono;

    @FXML
    private TableView<UsuariosProperty> tblUsuarios;

    @FXML
    private TextField tfBuscar;

    UsuariosAPI api = new UsuariosAPI();

    List<Usuarios> usuarios = new ArrayList<>();

    @FXML
    private void initialize() {

        ButtonStyle.buttonStyle(btNuevo, true);
        rellenaTabla();
    }

    private void rellenaTabla() {

        try {
            usuarios = api.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

//        tblUsuarios.getItems().clear();

        List<UsuariosProperty> usuariosPropertyList = new ArrayList<>();

        for (Usuarios u: usuarios) {
            usuariosPropertyList.add(new UsuariosProperty(u));
        }

        colNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        colEmail.setCellValueFactory(new PropertyValueFactory<>("email"));
        colTelefono.setCellValueFactory(new PropertyValueFactory<>("telefono"));
        colEditar.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        colEditar.setCellFactory(param -> new TableCell<>() {
            private final Button editarButton = new Button("Editar");


            @Override
            protected void updateItem(UsuariosProperty usuario, boolean empty) {
                super.updateItem(usuario, empty);

                if (usuario == null) {
                    setGraphic(null);
                    return;
                }

                setGraphic(editarButton);
                ButtonStyle.buttonStyle(editarButton, false);

                editarButton.setPrefWidth(Double.MAX_VALUE);
                    editarButton.setOnAction(event -> seeDetails(event, usuario.getId()));
            }
        });

        colBorrar.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        colBorrar.setCellFactory(param -> new TableCell<>() {
            private final Button borrarButton = new Button("Borrar");


            @Override
            protected void updateItem(UsuariosProperty usuario, boolean empty) {
                super.updateItem(usuario, empty);

                if (usuario == null) {
                    setGraphic(null);
                    return;
                }

                setGraphic(borrarButton);
                ButtonStyle.buttonStyle(borrarButton, false);

                borrarButton.setPrefWidth(Double.MAX_VALUE);
                borrarButton.setOnAction(event -> borrarUsuario(usuario.getId()));
            }
        });

        ObservableList<UsuariosProperty> data = FXCollections.observableArrayList(usuariosPropertyList);

        FilteredList<UsuariosProperty> filteredData = new FilteredList<>(data, a->true);

        tfBuscar.textProperty().addListener((observable, oldValue, newValue) -> filteredData.setPredicate(usuario -> {

            if (newValue == null || newValue.isEmpty())
                return true;

            String lowerCaseFilter = newValue.toLowerCase(Locale.ROOT);

            if (usuario.getNombre().toLowerCase(Locale.ROOT).contains(lowerCaseFilter)) {
                return true;
            } else {
                return false;
            }

        }));

        SortedList<UsuariosProperty> sortedData = new SortedList<>(filteredData);

        tblUsuarios.setItems(sortedData);
    }

    private void borrarUsuario(String id) {
        if (mensajeConfirmacion()) {
            tfBuscar.clear();
            api.delete(Integer.parseInt(id));
            rellenaTabla();
        }
    }

    private boolean mensajeConfirmacion() {

        try {
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("confirmacion.fxml"));
            Parent root = loader.load();
            ConfirmacionController controller = loader.getController();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setTitle("batoipop");
            stage.showAndWait();
            return controller.getOption();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return false;

    }

    public void createNew(ActionEvent event) {
        try {
            Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("nuevousuario.fxml"));
            Parent root = loader.load();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            scene.getStylesheets().addAll(Objects.requireNonNull(this.getClass().getResource("css/mainScreen.css")).toExternalForm());
            stage.setTitle("batoipop");
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void seeDetails(ActionEvent event, String usuario) {
        try {
            Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("nuevousuario.fxml"));
            Parent root = loader.load();
            NuevoUsuarioController controller = loader.getController();
            controller.setCurrentData(usuario);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            scene.getStylesheets().addAll(Objects.requireNonNull(this.getClass().getResource("css/mainScreen.css")).toExternalForm());
            stage.setTitle("batoipop");
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void goBack(MouseEvent event) {
        try {
            Parent root = FXMLLoader.load(Objects.requireNonNull(MainScreen.class.getResource("mainScreen.fxml")));
            Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            Scene scene = new Scene(root);
            scene.getStylesheets().addAll(Objects.requireNonNull(this.getClass().getResource("css/mainScreen.css")).toExternalForm());
            stage.setTitle("BatoiPop");
            stage.setFullScreen(true);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
