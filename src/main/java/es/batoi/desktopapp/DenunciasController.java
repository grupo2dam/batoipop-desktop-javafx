package es.batoi.desktopapp;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import modelo.articulos.Articulos;
import modelo.denuncias.Denuncias;
import modelo.denuncias.DenunciasProperty;
import modelo.usuarios.UsuariosProperty;
import style.ButtonStyle;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class DenunciasController {

    @FXML
    private ImageView btBack;

    @FXML
    private TableView<DenunciasProperty> tblDenuncias;

    @FXML
    private TableColumn<DenunciasProperty, String> colArticulo;

    @FXML
    private TableColumn<DenunciasProperty, String> colUsuario;

    @FXML
    private TableColumn<DenunciasProperty, String> colComentario;

    @FXML
    private TableColumn<DenunciasProperty, DenunciasProperty> colEditar;

    @FXML
    private TextField tfBuscar;

    List<Denuncias> denuncias = new ArrayList<>();
    Denuncias d1 = new Denuncias(1, "Daniel", "Se conserva en buen estado");
    Denuncias d2 = new Denuncias(2, "Margaret", "Esto da asco");

    @FXML
    private void initialize () {

        rellenaTabla();

    }

    private void rellenaTabla () {

        try {
            denuncias.add(d1);
            denuncias.add(d2);

            List<DenunciasProperty> denunciasPropertyList = new ArrayList<>();

            for (Denuncias de : denuncias) {
                denunciasPropertyList.add(new DenunciasProperty(de));
            }

            ObservableList<DenunciasProperty> data = FXCollections.observableArrayList(denunciasPropertyList);

            FilteredList<DenunciasProperty> filteredData = new FilteredList<>(data, a->true);

            colArticulo.setCellValueFactory(new PropertyValueFactory<>("articulo"));
            colUsuario.setCellValueFactory(new PropertyValueFactory<>("usuario"));
            colComentario.setCellValueFactory(new PropertyValueFactory<>("comentario"));
            colEditar.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
            colEditar.setCellFactory(param -> new TableCell<>() {
                private final Button editarButton = new Button("Ver detalles");


                @Override
                protected void updateItem(DenunciasProperty denuncia, boolean empty) {
                    super.updateItem(denuncia, empty);

                    if (denuncia == null) {
                        setGraphic(null);
                        return;
                    }

                    setGraphic(editarButton);
                    ButtonStyle.buttonStyle(editarButton, false);

                    editarButton.setPrefWidth(Double.MAX_VALUE);
                    editarButton.setOnAction(event -> seeDetails(event, denuncia.getArticulo()));
                }
            });

            tfBuscar.textProperty().addListener((observable, oldValue, newValue) -> filteredData.setPredicate(denuncia -> {

                if (newValue == null || newValue.isEmpty())
                    return true;

                String lowerCaseFilter = newValue.toLowerCase(Locale.ROOT);

                if (denuncia.getUsuario().toLowerCase(Locale.ROOT).contains(lowerCaseFilter)) {
                    return true;
                } else {
                    return false;
                }

            }));

            SortedList<DenunciasProperty> sortedData = new SortedList<>(filteredData);

            tblDenuncias.getItems().clear();
            tblDenuncias.setItems(sortedData);
        } catch (NullPointerException e) {
            tblDenuncias = new TableView<>();
        }
    }

    @FXML
    void seeDetails(ActionEvent event, String articulo) {
        try {
            Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("nuevoarticulo.fxml"));
            Parent root = loader.load();
            NuevoArticuloController controller = loader.getController();
            controller.setCurrentData(articulo);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            scene.getStylesheets().addAll(Objects.requireNonNull(this.getClass().getResource("css/mainScreen.css")).toExternalForm());
            stage.setTitle("batoipop");
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void goBack(MouseEvent event) {
        try {
            Parent root = FXMLLoader.load(Objects.requireNonNull(MainScreen.class.getResource("mainScreen.fxml")));
            Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            Scene scene = new Scene(root);
            scene.getStylesheets().addAll(Objects.requireNonNull(this.getClass().getResource("css/mainScreen.css")).toExternalForm());
            stage.setTitle("BatoiPop");
            stage.setFullScreen(true);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
