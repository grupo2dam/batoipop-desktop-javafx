package es.batoi.desktopapp;

import api.UsuariosAPI;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import modelo.usuarios.Usuarios;
import style.ButtonStyle;

import java.io.IOException;
import java.util.Objects;

public class NuevoUsuarioController {

    public static final int INSERT = 0;
    public static final int UPDATE = 1;

    private int method = INSERT;

    @FXML
    private Button btDescartar;

    @FXML
    private Button btGuardar;

    @FXML
    private TextField tfContrasenya;

    @FXML
    private TextField tfEmail;

    @FXML
    private TextField tfId;

    @FXML
    private TextField tfNombre;

    @FXML
    private TextField tfTelefono;

    private UsuariosAPI usuariosAPI = new UsuariosAPI();

    private int currentId = 0;

    @FXML
    private void initialize () {
        ButtonStyle.buttonStyle(btDescartar, true);
        ButtonStyle.buttonStyle(btGuardar, true);
    }

    @FXML
    void saveUser(MouseEvent event) {

        if (method == INSERT) {
            usuariosAPI.insert(getUser());
        } else if (method == UPDATE) {
            usuariosAPI.update(getUser());
        }
        goBack(event);

    }

    public void goBack(MouseEvent event) {
        try {
            Parent root = FXMLLoader.load(Objects.requireNonNull(MainScreen.class.getResource("usuarios.fxml")));
            Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            Scene scene = new Scene(root);
            scene.getStylesheets().addAll(Objects.requireNonNull(this.getClass().getResource("css/mainScreen.css")).toExternalForm());
            stage.setTitle("BatoiPop");
            stage.setFullScreen(true);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void setCurrentData(String usuario) {

        method = UPDATE;

        currentId = Integer.parseInt(usuario);
        tfId.setText(usuario);
        Usuarios u = null;
        try {
            u = usuariosAPI.findByPK(currentId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assert u != null;
        tfNombre.setText(u.getNombre());
        tfEmail.setText(u.getEmail());
        tfTelefono.setText(u.getTelefono());
        tfContrasenya.setText(u.getContrasena());

    }

    private Usuarios getUser() {


        String nombre = tfNombre.getText();
        String email = tfEmail.getText();
        String telefono = tfTelefono.getText();
        String contrasena = tfContrasenya.getText();


        if (method == INSERT)
            return new Usuarios(nombre, email, telefono, contrasena);
        else if (method == UPDATE) {
            int id = Integer.parseInt(tfId.getText());
            return new Usuarios(id, nombre, email, telefono, contrasena);
        } else
            return null;

    }

}
