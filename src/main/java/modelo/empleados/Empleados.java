package modelo.empleados;

import java.io.Serializable;

public class Empleados implements Serializable {
    public Integer id;
    public String username;
    public String password;

    public Empleados(Integer id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public Empleados(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Empleados{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
