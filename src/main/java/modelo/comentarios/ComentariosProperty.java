package modelo.comentarios;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ComentariosProperty {

    private StringProperty articulo;
    private StringProperty usuario;
    private StringProperty comentario;

    public ComentariosProperty(Comentarios comentario) {

        articulo = new SimpleStringProperty(comentario.getArticulo() + "");
        usuario = new SimpleStringProperty(comentario.getUsuario() + "");
        this.comentario = new SimpleStringProperty(comentario.getMensaje() + "");

    }

    public String getArticulo() {
        return articulo.get();
    }

    public StringProperty articuloProperty() {
        return articulo;
    }

    public void setArticulo(String articulo) {
        this.articulo.set(articulo);
    }

    public String getUsuario() {
        return usuario.get();
    }

    public StringProperty usuarioProperty() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario.set(usuario);
    }

    public String getComentario() {
        return comentario.get();
    }

    public StringProperty comentarioProperty() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario.set(comentario);
    }
}
