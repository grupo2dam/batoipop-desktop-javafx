package modelo.articulos;

import modelo.etiquetas.Etiquetas;

import java.util.ArrayList;

public class Articulos {

    private int id;
    private String nombre;
    private String descripcion;
    private String antiguedad;
    private float precio;
    private ArrayList<Etiquetas> batoipopEtiquetas;

    public Articulos(int id, String nombre, String descripcion, String antiguedad, float precio) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.antiguedad = antiguedad;
        this.precio = precio;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getAntiguedad() {
        return antiguedad;
    }

    public void setAntiguedad(String antiguedad) {
        this.antiguedad = antiguedad;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "Articulos{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", antiguedad='" + antiguedad + '\'' +
                ", precio=" + precio +
                ", batoipopEtiquetas=" + batoipopEtiquetas +
                '}';
    }
}
