package modelo.categorias;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class CategoriasProperty {

    private StringProperty id;
    private StringProperty nombre;
    private StringProperty descripcion;

    public CategoriasProperty (Categorias cat) {
        id = new SimpleStringProperty(cat.getId() + "");
        nombre = new SimpleStringProperty(cat.getNombre());
        descripcion = new SimpleStringProperty(cat.getDescripcion());
    }

    public String getId() {
        return id.get();
    }

    public StringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getNombre() {
        return nombre.get();
    }

    public StringProperty nombreProperty() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }

    public String getDescripcion() {
        return descripcion.get();
    }

    public StringProperty descripcionProperty() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion.set(descripcion);
    }
}
