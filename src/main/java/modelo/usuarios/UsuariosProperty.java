package modelo.usuarios;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class UsuariosProperty {

    private StringProperty id;
    private StringProperty nombre;
    private StringProperty telefono;
    private StringProperty email;

    public UsuariosProperty (Usuarios user) {
        id = new SimpleStringProperty(user.getId() + "");
        nombre = new SimpleStringProperty(user.getNombre());
        telefono = new SimpleStringProperty(user.getTelefono());
        email = new SimpleStringProperty(user.getEmail());
    }

    public String getId() {
        return id.get();
    }

    public StringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getNombre() {
        return nombre.get();
    }

    public StringProperty nombreProperty() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }

    public String getTelefono() {
        return telefono.get();
    }

    public StringProperty telefonoProperty() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono.set(telefono);
    }

    public String getEmail() {
        return email.get();
    }

    public StringProperty emailProperty() {
        return email;
    }

    public void setEmail(String email) {
        this.email.set(email);
    }
}
