package modelo.denuncias;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class DenunciasProperty {

    private StringProperty articulo;
    private StringProperty usuario;
    private StringProperty comentario;

    public DenunciasProperty(Denuncias denuncia) {

        articulo = new SimpleStringProperty(denuncia.getArticulo() + "");
        usuario = new SimpleStringProperty(denuncia.getUsuario() + "");
        comentario = new SimpleStringProperty(denuncia.getMensaje() + "");

    }

    public String getArticulo() {
        return articulo.get();
    }

    public StringProperty articuloProperty() {
        return articulo;
    }

    public void setArticulo(String articulo) {
        this.articulo.set(articulo);
    }

    public String getUsuario() {
        return usuario.get();
    }

    public StringProperty usuarioProperty() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario.set(usuario);
    }

    public String getComentario() {
        return comentario.get();
    }

    public StringProperty comentarioProperty() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario.set(comentario);
    }
}
