package modelo.denuncias;

import javafx.fxml.FXML;

import javafx.scene.control.TableColumn;

public class Denuncias {
    private int articulo;
    private String usuario;
    private String mensaje;



    public Denuncias(int articulo, String usuario, String mensaje) {
        this.articulo = articulo;
        this.usuario = usuario;
        this.mensaje = mensaje;
    }

    public int getArticulo() {
        return articulo;
    }

    public void setArticulo(int articulo) {
        this.articulo = articulo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
