package modelo.etiquetas;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class EtiquetasProperty {

    private StringProperty id;
    private StringProperty nombre;

    public EtiquetasProperty(Etiquetas e) {
        id = new SimpleStringProperty(e.getId() + "");
        nombre = new SimpleStringProperty(e.getNombre());
    }

    public String getId() {
        return id.get();
    }

    public StringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getNombre() {
        return nombre.get();
    }

    public StringProperty nombreProperty() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }
}
