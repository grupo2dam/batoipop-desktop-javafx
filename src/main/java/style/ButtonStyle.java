package style;

import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.Button;

public class ButtonStyle {

    private static final String BUTTON_STYLE_NORMAL = "-fx-background-color: #A60707; -fx-text-fill: white; -fx-font-family: Roundo; -fx-background-radius: 5px;";
    private static final String BUTTON_STYLE_HOVER = "-fx-background-color: #8B0000; -fx-text-fill: white; -fx-font-family: Roundo; -fx-background-radius: 5px;";

    public static void buttonStyle (Button button, boolean fontSizeChange) {

        String fontSize = "";

        if (fontSizeChange) {
            fontSize = "-fx-font-size: 20px";
        }

        button.styleProperty().bind(Bindings
                .when(button.hoverProperty())
                .then(
                        new SimpleStringProperty(BUTTON_STYLE_HOVER + fontSize)
                )
                .otherwise(
                        new SimpleStringProperty(BUTTON_STYLE_NORMAL + fontSize)
                ));
    }

}
