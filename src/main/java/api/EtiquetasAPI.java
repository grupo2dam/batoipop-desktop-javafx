package api;

import com.google.gson.Gson;
import modelo.comentarios.Comentarios;
import modelo.etiquetas.Etiquetas;
import modelo.usuarios.Usuarios;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EtiquetasAPI implements InterfazAPI<Etiquetas> {

    private final String TABLE = "/etiqueta";
    private final String TABLE_URL = DatosAPI.BASE_URL + TABLE;

    @Override
    public Etiquetas findByPK(int id) {
        return null;
    }

    @Override
    public List<Etiquetas> findAll() {
        String lin, salida = "";
        Etiquetas[] etiquetas;
        List<Etiquetas> listEtiquetas = null;
        Gson gson;
        URL url;
        HttpURLConnection con;

        try {
            url = new URL( TABLE_URL + "/all");
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod(DatosAPI.GET);
            con.setRequestProperty("Accept", "application/json");

            if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
                // System.out.println("Fallo leyendo recurso. Código de error: " +
                // con.getResponseCode());
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            } else {
                BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
                while ((lin = br.readLine()) != null) {
                    salida = salida.concat(lin);
                }

                gson = new Gson();
                etiquetas = gson.fromJson(salida, Etiquetas[].class);
                listEtiquetas = new ArrayList<>();
                listEtiquetas = Arrays.asList(etiquetas);
//				for (int i = 0; i < clientes.length; i++) {
//					listClientes.add(clientes[i]);
//				}
                con.disconnect();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return listEtiquetas;
    }

    @Override
    public void insert(Etiquetas etiqueta) {
        URL url;
        HttpURLConnection con;
        try {
            url = new URL(TABLE_URL);
            con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");

            Gson gson = new Gson();
            String etiquetaString = gson.toJson(etiqueta, Etiquetas.class);

            OutputStream os = con.getOutputStream();
            os.write(etiquetaString.getBytes());
            os.flush();

            Reader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            for (int c = in.read(); c != -1; c = in.read())
                System.out.print((char) c);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Etiquetas etiqueta) {

    }

    @Override
    public void delete(int id) {
        URL url;
        HttpURLConnection con;

        try {
            url = new URL(TABLE_URL + "/" + id);
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("DELETE");

            if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }
            con.disconnect();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
