package api;

import com.google.gson.Gson;
import modelo.categorias.Categorias;
import modelo.usuarios.Usuarios;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CategoriasAPI implements InterfazAPI<Categorias> {

    private final String TABLE = "/categoria";
    private final String TABLE_URL = DatosAPI.BASE_URL + TABLE;

    @Override
    public Categorias findByPK(int id) {
        String lin, salida = "";
        Categorias categorias = null;
        Gson gson;
        URL url;
        HttpURLConnection con;

        try {
            url = new URL( TABLE_URL+ "?id=" + id);
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept", "application/json");

            if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
                // System.out.println("Fallo leyendo recurso. Código de error: " +
                // con.getResponseCode());
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            } else {
                BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
                while ((lin = br.readLine()) != null) {
                    salida = salida.concat(lin);
                }
                gson = new Gson();
                categorias = gson.fromJson(salida, Categorias.class);
                con.disconnect();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return categorias;
    }

    @Override
    public List<Categorias> findAll() {
        String lin, salida = "";
        Categorias[] categorias;
        List<Categorias> listCategorias = null;
        Gson gson;
        URL url;
        HttpURLConnection con;

        try {
            url = new URL( TABLE_URL + "/all");
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod(DatosAPI.GET);
            con.setRequestProperty("Accept", "application/json");

            if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
                // System.out.println("Fallo leyendo recurso. Código de error: " +
                // con.getResponseCode());
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            } else {
                BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
                while ((lin = br.readLine()) != null) {
                    salida = salida.concat(lin);
                }

                gson = new Gson();
                categorias = gson.fromJson(salida, Categorias[].class);
                listCategorias = new ArrayList<>();
                listCategorias = Arrays.asList(categorias);
//				for (int i = 0; i < clientes.length; i++) {
//					listClientes.add(clientes[i]);
//				}
                con.disconnect();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return listCategorias;
    }

    @Override
    public void insert(Categorias categoria) {
        URL url;
        HttpURLConnection con;
        try {
            url = new URL(TABLE_URL);
            con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");

            Gson gson = new Gson();
            String categoriaString = gson.toJson(categoria, Categorias.class);

            OutputStream os = con.getOutputStream();
            os.write(categoriaString.getBytes());
            os.flush();

            Reader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            for (int c = in.read(); c != -1; c = in.read())
                System.out.print((char) c);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Categorias categoria) {
        URL url;
        HttpURLConnection con;
        try {

            url = new URL(TABLE_URL + "/" + categoria.getId());
            con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("PUT");
            con.setRequestProperty("Content-Type", "application/json");

            categoria.setId(0);

            Gson gson = new Gson();
            String usuarioString = gson.toJson(categoria, Categorias.class);

            OutputStream os = con.getOutputStream();
            os.write(usuarioString.getBytes());
            os.flush();

            Reader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            for (int c = in.read(); c != -1; c = in.read())
                System.out.print((char) c);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(int id) {
        URL url;
        HttpURLConnection con;

        try {
            url = new URL(TABLE_URL + "/" + id);
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("DELETE");

            if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }
            con.disconnect();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
