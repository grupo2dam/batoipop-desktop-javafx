package api;

public class DatosAPI {


    public static final String IP_ADDRESS = "137.74.226.42";
    public static final int PORT = 8080;
    public static final String BASE_URL = "http://" + IP_ADDRESS + ":" + PORT;

    public static final String GET = "GET";
    public static final String POST = "POST";
    public static final String PUT = "PUT";
    public static final String DELETE = "DELETE";

}
