package api;

import java.util.List;

public interface InterfazAPI <Tipo> {

    Tipo findByPK (int id);
    List<Tipo> findAll();
    void insert(Tipo t);
    void update(Tipo t);
    void delete(int id);

}
