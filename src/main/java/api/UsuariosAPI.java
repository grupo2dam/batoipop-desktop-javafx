package api;

import com.google.gson.Gson;
import modelo.empleados.Empleados;
import modelo.usuarios.Usuarios;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UsuariosAPI implements InterfazAPI<Usuarios> {

    private final String TABLE = "/usuario";
    private final String TABLE_URL = DatosAPI.BASE_URL + TABLE;

    @Override
    public Usuarios findByPK(int id){
        String lin, salida = "";
        Usuarios usuario = null;
        Gson gson;
        URL url;
        HttpURLConnection con;

        try {
            url = new URL( TABLE_URL+ "?id=" + id);
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept", "application/json");

            if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
                // System.out.println("Fallo leyendo recurso. Código de error: " +
                // con.getResponseCode());
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            } else {
                BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
                while ((lin = br.readLine()) != null) {
                    salida = salida.concat(lin);
                }
                gson = new Gson();
                usuario = gson.fromJson(salida, Usuarios.class);
                con.disconnect();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return usuario;
    }

    @Override
    public List<Usuarios> findAll(){
        String lin, salida = "";
        Usuarios[] usuarios;
        List<Usuarios> listUsuarios = null;
        Gson gson;
        URL url;
        HttpURLConnection con;

        try {
            url = new URL( TABLE_URL + "/all");
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod(DatosAPI.GET);
            con.setRequestProperty("Accept", "application/json");

            if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
                // System.out.println("Fallo leyendo recurso. Código de error: " +
                // con.getResponseCode());
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            } else {
                BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
                while ((lin = br.readLine()) != null) {
                    salida = salida.concat(lin);
                }

                gson = new Gson();
                usuarios = gson.fromJson(salida, Usuarios[].class);
                listUsuarios = new ArrayList<>();
                listUsuarios = Arrays.asList(usuarios);
//				for (int i = 0; i < clientes.length; i++) {
//					listClientes.add(clientes[i]);
//				}
                con.disconnect();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return listUsuarios;
    }

    @Override
    public void insert(Usuarios usuario){
        URL url;
        HttpURLConnection con;
        try {
            url = new URL(TABLE_URL);
            con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");

            Gson gson = new Gson();
            String usuarioString = gson.toJson(usuario, Usuarios.class);

            OutputStream os = con.getOutputStream();
            os.write(usuarioString.getBytes());
            os.flush();

            Reader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            for (int c = in.read(); c != -1; c = in.read())
                System.out.print((char) c);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Usuarios usuario){
        URL url;
        HttpURLConnection con;
        try {

            url = new URL(TABLE_URL + "/" + usuario.getId());
            con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("PUT");
            con.setRequestProperty("Content-Type", "application/json");

            usuario.setId(0);

            Gson gson = new Gson();
            String usuarioString = gson.toJson(usuario, Usuarios.class);

            OutputStream os = con.getOutputStream();
            os.write(usuarioString.getBytes());
            os.flush();

            Reader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            for (int c = in.read(); c != -1; c = in.read())
                System.out.print((char) c);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(int id){
        URL url;
        HttpURLConnection con;

        try {
            url = new URL(TABLE_URL + "/" + id);
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("DELETE");

            if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }
            con.disconnect();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
