package api;

import com.google.gson.Gson;
import modelo.empleados.Empleados;
import modelo.usuarios.Usuarios;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class EmpleadoAPI implements InterfazAPI<Empleados> {

    private final String TABLE = "/empleado";
    private final String TABLE_URL = DatosAPI.BASE_URL + TABLE;

    @Override
    public Empleados findByPK(int id) {
        return null;
    }

    @Override
    public List<Empleados> findAll() {
        return null;
    }

    @Override
    public void insert(Empleados emp) {
        URL url;
        HttpURLConnection con;
        try {
            url = new URL(TABLE_URL);
            con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");

            Gson gson = new Gson();
            String empleadoString = gson.toJson(emp, Empleados.class);

            OutputStream os = con.getOutputStream();
            os.write(empleadoString.getBytes());
            os.flush();


            Reader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            for (int c = in.read(); c != -1; c = in.read())
                System.out.print((char) c);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Empleados t) {

    }

    @Override
    public void delete(int id) {

    }

    public Empleados login(String nombreUsuario, String contrasena) {
        String lin, salida = "";
        Empleados empleado = null;
        Gson gson;
        URL url;
        HttpURLConnection con;
        try {
            url = new URL(TABLE_URL + "?u=" + nombreUsuario + "&p=" + contrasena);
            con = (HttpURLConnection) url.openConnection();

            con.setRequestMethod(DatosAPI.GET);
            //con.setRequestProperty("Accept", "application/json");

            if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("El nombre y la contraseña deben ser correctos");
            } else {
                BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
                while ((lin = br.readLine()) != null) {
                    salida = salida.concat(lin);
                }
                gson = new Gson();
                empleado = gson.fromJson(salida, Empleados.class);
                con.disconnect();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return empleado;
    }
}
