package api;

import com.google.gson.Gson;
import modelo.articulos.Articulos;
import modelo.usuarios.Usuarios;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArticulosAPI implements InterfazAPI<Articulos> {

    private final String TABLE = "/articulo";
    private final String TABLE_URL = DatosAPI.BASE_URL + TABLE;


    @Override
    public Articulos findByPK(int id){
        String lin, salida = "";
        Articulos articulos = null;
        Gson gson;
        URL url;
        HttpURLConnection con;

        try {
            url = new URL( TABLE_URL+ "/id?id=" + id);
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept", "application/json");

            if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
                // System.out.println("Fallo leyendo recurso. Código de error: " +
                // con.getResponseCode());
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            } else {
                BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
                while ((lin = br.readLine()) != null) {
                    salida = salida.concat(lin);
                }
                gson = new Gson();
                articulos = gson.fromJson(salida, Articulos.class);
                con.disconnect();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return articulos;
    }

    @Override
    public List<Articulos> findAll(){
        String lin, salida = "";
        Articulos[] articulos;
        List<Articulos> listClientes = null;
        Gson gson;
        URL url;
        HttpURLConnection con;

        try {
            url = new URL( TABLE_URL + "/all");
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod(DatosAPI.GET);
            con.setRequestProperty("Accept", "application/json");

            if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
                // System.out.println("Fallo leyendo recurso. Código de error: " +
                // con.getResponseCode());
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            } else {
                BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
                while ((lin = br.readLine()) != null) {
                    salida = salida.concat(lin);
                }

                gson = new Gson();
                articulos = gson.fromJson(salida, Articulos[].class);
                listClientes = new ArrayList<>();
                listClientes = Arrays.asList(articulos);
//				for (int i = 0; i < clientes.length; i++) {
//					listClientes.add(clientes[i]);
//				}
                con.disconnect();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return listClientes;
    }

    @Override
    public void insert(Articulos articulo){
        URL url;
        HttpURLConnection con;
        try {
            url = new URL(TABLE_URL);
            con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");

            Gson gson = new Gson();
            String usuarioString = gson.toJson(articulo, Articulos.class);

            OutputStream os = con.getOutputStream();
            os.write(usuarioString.getBytes());
            os.flush();

            Reader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            for (int c = in.read(); c != -1; c = in.read())
                System.out.print((char) c);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Articulos t){

    }

    @Override
    public void delete(int id){

    }
}
