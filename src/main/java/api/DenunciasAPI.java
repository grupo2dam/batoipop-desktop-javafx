package api;

import com.google.gson.Gson;
import modelo.comentarios.Comentarios;
import modelo.denuncias.Denuncias;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class DenunciasAPI implements InterfazAPI<Denuncias> {

    private final String TABLE = "/denuncia";
    private final String TABLE_URL = DatosAPI.BASE_URL + TABLE;

    @Override
    public Denuncias findByPK(int id) {
        return null;
    }

    @Override
    public List<Denuncias> findAll() {
        return null;
    }

    @Override
    public void insert(Denuncias denuncia) {
        URL url;
        HttpURLConnection con;
        try {
            url = new URL(TABLE_URL);
            con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");

            Gson gson = new Gson();
            String usuarioString = gson.toJson(denuncia, Denuncias.class);

            OutputStream os = con.getOutputStream();
            os.write(usuarioString.getBytes());
            os.flush();

            Reader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            for (int c = in.read(); c != -1; c = in.read())
                System.out.print((char) c);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Denuncias denuncia) {

    }

    @Override
    public void delete(int id) {

    }
}
