module es.batoi.desktopapp {
    requires javafx.controls;
    requires javafx.fxml;
    requires com.google.gson;


    opens es.batoi.desktopapp to javafx.fxml;
    opens modelo.articulos to com.google.gson;
    opens modelo.etiquetas to com.google.gson;
    opens modelo.comentarios to com.google.gson;
    opens modelo.denuncias to com.google.gson;
    opens modelo.usuarios to com.google.gson;
    exports es.batoi.desktopapp;
    exports modelo.categorias;
    exports modelo.comentarios;
    exports modelo.denuncias;
    exports modelo.etiquetas;
    exports modelo.usuarios;
    exports modelo.articulos;
    exports api;
}